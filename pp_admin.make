; Admin enhancements modules

api = 2
core = 7.x

projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc4"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "1.8"

projects[admin_views][subdir] = "contrib"
projects[admin_views][version] = "1.2"
